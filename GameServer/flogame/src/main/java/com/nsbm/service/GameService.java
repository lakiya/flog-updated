/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nsbm.service;

import static com.nsbm.common.CommonUtil.checkPlayable;
import com.nsbm.common.CurrentPlay;
import static com.nsbm.common.ResponseResult.SUCCESS;
import com.nsbm.entity.Player;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Lakshitha
 */
@Path("/GameService")
public class GameService {
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/startGame")
    public String startGame(Player player) {
        return checkPlayable();
    }
    
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/resetGame")
    public String resetGame() {
        CurrentPlay.currentRound = 0;
        return SUCCESS;
    }
    
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/resetFile")
    public String resetFile() {
        PrintWriter pw = null;
        String dataPath = System.getenv("OPENSHIFT_DATA_DIR");
        try {
            pw = new PrintWriter(dataPath +"/Players.txt");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GameService.class.getName()).log(Level.SEVERE, null, ex);
        }
        pw.close();
        return SUCCESS;
    }
}
